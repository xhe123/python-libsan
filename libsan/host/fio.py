# Copyright (C) 2016 Red Hat, Inc.
# This file is part of libsan.
#
# libsan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libsan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libsan.  If not, see <http://www.gnu.org/licenses/>.


"""fio.py: Module to run FIO util."""

__author__      = "Bruno Goncalves"
__copyright__   = "Copyright (c) 2016 Red Hat, Inc. All rights reserved."

from libsan.host.cmdline import run
import libsan.misc.time as time
import libsan.host.linux as linux
import re #regex
import sys, os, subprocess

FIO_DEFAULT_OPTION = {"rw":             "randrw",   #Type of I/O pattern. Supported (read, write, trim, randread, randwrite, rw, randrw, trimwrite)
                      "name":           "fio_test",  #signalling the start of a new job.
                      "filename":       None,       #device or filename
                      "direct":         1,          #If true, use non-buffered I/O (usually O_DIRECT)
                      "iodepth":        1,          #Number  of  I/O  units  to keep in flight against the file. Note that increasing iodepth beyond 1 will not affect synchronous ioengines
                      "runtime":        0,          #Terminate processing after the specified number of seconds.
                      "size":           None,       #
                      "time_based":     None,       #If given, run for the specified runtime duration even if the files are completely read or written.
                      "numjobs":        1,          #Number of clones (processes/threads performing the same workload) of this job
                      "bs":             "4k",       #lock  size for I/O units.
                      "verify":         None        #Method  of verifying file contents after each iteration of the job (supports: md5 crc16 crc32 crc32c crc32c-intel crc64 crc7 sha256 sha512 sha1 xxhash)
                      }
FIO_DEFAULT_VERIFY_OPTION = {
                      "verify_backlog": 1024,       #fio will write only N blocks before verifying these blocks. Set to None to verify after all IO is written
                      "verify_fatal":   1,          #If true, exit the job on the first observed verification failure
                      "do_verify": 1
                      }


def _print(string):
    module_name = __name__
    string = re.sub("FAIL:", "FAIL:("+ module_name + ") ", string)
    string = re.sub("FATAL:", "FATAL:("+ module_name + ") ", string)
    string = re.sub("WARN:", "WARN:("+ module_name + ") ", string)
    print string
    sys.stdout.flush()
    return

def _has_fio():
    if run("which fio", verbose = False) != 0:
        return False
    return True

def install_fio():
    pkg = "fio"
    if linux.install_package(pkg):
        return True

    #Try to install FIO from source
    return install_fio_from_src()

def install_fio_from_src():
    git_url = "git://git.kernel.org/pub/scm/linux/kernel/git/axboe/fio.git"


    if not linux.install_package("libaio-devel"):
        _print("FAIL: Could not install libaio-devel")
        return False

    if not linux.install_package("zlib-devel"):
        _print("FAIL: Could not install zlib-devel")
        return False

    if run("git clone %s" % git_url) != 0:
        _print("FAIL: Could not clone fio repo")
        return False

    _print("INFO: Installing FIO")
    if run("cd fio && ./configure && make && make install") != 0:
        _print("FAIL: Could not build fio")
        return False

    if not _has_fio():
        _print("FAIL: FIO did not install properly")
        return False
    return True

def fio_stress(of, io_type=None, direct=None, iodepth=None, runtime=None,
              size=None, time_based=True, threads=None, bs=None, verify=None,
              verify_backlog=None, verify_fatal=None,
              verbose=False, return_output=False):
    global FIO_DEFAULT_OPTION
    global FIO_DEFAULT_VERIFY_OPTION

    fio_opt = FIO_DEFAULT_OPTION
    fio_opt["filename"] = of
    if io_type:
        fio_opt["rw"] = io_type
    if direct:
        fio_opt["direct"] = direct
    if iodepth:
        fio_opt["iodepth"] = iodepth
    if runtime:
        fio_opt["runtime"] = time.time_2_sec(runtime)
        if time_based:
            fio_opt["time_based"] = 1
    if size:
        fio_opt["size"] = size
    if threads:
        fio_opt["numjobs"] = threads
    if int(fio_opt["numjobs"]) > 1:
        fio_opt["group_reporting"] = 1
    if bs:
        fio_opt["bs"] = bs
    if verify:
        fio_opt["verify"] = verify
        #We need to add default options to verify case
        fio_opt.update(FIO_DEFAULT_VERIFY_OPTION)

        if verify_backlog != None:
            fio_opt["verify_backlog"] = verify_backlog
        if verify_fatal != None:
            fio["verify_fatal"] = verify_fatal

    if not _has_fio():
        _print("FATAL: fio is not installed")
        return False

    fio_param = ""
    for key in fio_opt.keys():
        if fio_opt[key]:
            fio_param += "--%s='%s' " % (key, fio_opt[key])

    if not re.match("/dev/", fio_opt["filename"]):
        #It does not seem a block device, assume it is a file
        #Run the job using just the file settings
        fio_opt = {}
        fio_param = of

    cmd = "fio %s" % (fio_param)
    #Append time information to command
    date = "date \"+%Y-%m-%d %H:%M:%S\""
    p = subprocess.Popen(date, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = p.communicate()
    stdout = stdout.rstrip("\n")
    _print("INFO: [%s] FIO Running: '%s'..." % (stdout, cmd))

    #_print("INFO: Running %s" % cmd)
    retcode, output = run(cmd, return_output = True, verbose = verbose)
    if (retcode != 0):
        _print ("FAIL: running FIO")
        print output
        if return_output:
            return False, None
        return False

    _print("INFO: FIO executed successfully")
    if return_output:
        return True, output

    return True


def fio_stress_background(of, io_type=None, direct=None, iodepth=None, runtime=None,
              size=None, time_based=True, threads=None, bs=None, verify=None,
              verify_backlog=None, verify_fatal=None,
              verbose=False, return_output=False):
    """Run FIO on background"""
    newpid = os.fork()
    if newpid == 0:
        #Trying to flush stdout to avoid duplicated lines when running hba_test
        sys.stdout.flush()
        rt = fio_stress(of, io_type = io_type, direct = direct, iodepth = iodepth,
                        runtime = runtime, time_based = time_based, threads = threads,
                        bs = bs, verify = verify, verify_backlog = verify_backlog, verify_fatal = verify_fatal,
                        verbose = verbose)
        if not rt:
            sys.exit(1)
        sys.exit(0)
    else:
        sys.stdout.flush()
        _print("INFO: fio_stress_background(): Child thread %d is running FIO Stress" % newpid)
        return newpid

