# Copyright (C) 2016 Red Hat, Inc.
# This file is part of libsan.
#
# libsan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libsan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libsan.  If not, see <http://www.gnu.org/licenses/>.

"""sanmgmt.py: Module to help manage SAN devices that are conifgured at /etc/san_top.conf."""

__author__      = "Bruno Goncalves"
__copyright__   = "Copyright (c) 2016 Red Hat, Inc. All rights reserved."

from libsan.host.cmdline import run
from libsan.misc.size import size_human_check, size_human_2_size_bytes
from libsan.host.conf import load_config, get_alias
import libsan.host.mp as mp
import libsan.host.fc as fc
import libsan.host.iscsi as iscsi
import libsan.host.scsi as scsi
import libsan.host.net as net
import libsan.host.linux as linux
import libsan.host.lvm as lvm
import libsan.misc.array as array
import libsan.switch.cisco.nxos as nxos
import libsan.switch.brocade.fos as fos
import os.path
import sys
import re #regex

DEFAULT_CONF = "/etc/san_top.conf"

def _print(string):
    module_name = __name__
    string = re.sub("DEBUG:", "DEBUG:("+ module_name + ") ", string)
    string = re.sub("FAIL:", "FAIL:("+ module_name + ") ", string)
    string = re.sub("FATAL:", "FATAL:("+ module_name + ") ", string)
    string = re.sub("WARN:", "WARN:("+ module_name + ") ", string)
    print string
    if "FATAL:" in string:
        raise RuntimeError(string)
    return

def create_sanmgmt_for_mpath(mpath_name=None, san_conf = None):
    """
    Usage
        create_sanmgmt_for_mpath($mpath_name)
    Purpose
        Create SanMgmt object base on information of mpath mpath, will load
        the default configuration of SanMgmt via obj_sanmgmt->load_conf()
    Parameter
        mpath_name     # like 'mpatha'
            or
        undef           # will return the default object of libsan
    Returns
        libsan_obj     # object of libsan
            or
        undef
    """
    obj_sanmgmt = SanMgmt()
    h_wwpns = []
    t_wwpns = []
    t_iqns = []
    h_iqns = []
    iface_macs = []

    if mpath_name:
        mpath_info_dict = mp.multipath_query_all(mpath_name)
        if not mpath_info_dict:
            _print("FAIL: Could not find info for multipath %s" % mpath_name)
            return None
        if "t_wwpns" in mpath_info_dict.keys():
            t_wwpns = mpath_info_dict["t_wwpns"]
        if "h_wwpns" in mpath_info_dict.keys():
            h_wwpns = mpath_info_dict["h_wwpns"]
        if "t_iqns" in mpath_info_dict.keys():
            t_iqns = mpath_info_dict["t_iqns"]
        if "h_iqns" in mpath_info_dict.keys():
            h_iqns = mpath_info_dict["h_iqns"]
        if "iface_macs" in mpath_info_dict.keys():
            iface_macs = mpath_info_dict["iface_macs"]
        #Map info well be used to create tmp LUN with the same settings as mpath_name
        if "map_info" in mpath_info_dict.keys():
            obj_sanmgmt.map_info(mpath_info_dict["map_info"])

    else:
        h_wwpns = fc.h_wwpn_of_host()
        t_wwpns = fc.t_wwpn_of_host()
        h_iqns = iscsi.h_iqn_of_sessions()
        t_iqns = iscsi.t_iqn_of_sessions()
        iface_macs = iscsi.mac_of_iscsi_session()

    if t_wwpns and h_wwpns:
        obj_sanmgmt.h_wwpns(h_wwpns)
        obj_sanmgmt.t_wwpns(t_wwpns)
    if t_iqns and h_iqns:
        obj_sanmgmt.h_iqns(h_iqns)
        obj_sanmgmt.t_iqns(t_iqns)
        obj_sanmgmt.macs(iface_macs)


    obj_sanmgmt.load_conf(san_conf)
    return obj_sanmgmt

def create_sanmgmt_for_storage_array(storage_name, san_conf = None):
    """
    Usage
        create_sanmgmt_for_storage_array(storage_name)
    Purpose
        Create SanMgmt object base on information of mpath mpath, will load
        the default configuration of SanMgmt via obj_sanmgmt->load_conf()
    Parameter
        mpath_name     # like 'mpatha'
            or
        undef           # will return the default object of libsan
    Returns
        libsan_obj     # object of libsan
            or
        undef
    """

    san_conf_list = _load_san_conf(san_conf)
    if not san_conf_list:
        _print("FAIL: create_sanmgmt_for_storage_array() - Could read san config")
        return None
    sa_conf = None

    for san_cfg in san_conf_list:
        san_dev = san_cfg["name"]
        if "options" not in san_cfg.keys():
            continue
        dev_conf_dict = san_cfg["options"]

        #We only want type of storage array
        if ("type" not in dev_conf_dict.keys() or
            dev_conf_dict["type"] != "array"):
            continue
        if dev_conf_dict["ctrl_ip"] == storage_name:
            sa_conf = dev_conf_dict
            break

    if not sa_conf:
        _print("FAIL: create_sanmgmt_for_storage_array() - Could not find storage array with name %s" % storage_name)
        return None

    obj_sanmgmt = SanMgmt()
    t_iqns = []
    t_wwpns = []
    if "port_of" in sa_conf.keys():
        for port in sa_conf["port_of"].keys():
            if iscsi.is_iqn(port):
                t_iqns.append(port)
            if fc.is_wwn(port):
                t_wwpns.append(port)
    if t_iqns:
        obj_sanmgmt.t_iqns(t_iqns)
    if t_wwpns:
        obj_sanmgmt.t_wwpns(t_wwpns)

    obj_sanmgmt.load_conf(san_conf)
    return obj_sanmgmt

def _load_san_conf(san_conf = None):
    """
    Load the SAN storage configuration from config file
    default path for config file is /etc/san_top.conf
    """
    global DEFAULT_CONF
    if not san_conf:
        san_conf = DEFAULT_CONF

    if not os.path.isfile(san_conf):
        _print("FAIL: Could not read %s" % san_conf)
        return None

    _print("INFO: SanMgmt Loading conf: %s" % san_conf)
    san_conf_list = load_config(san_conf)
    module_name_dict = {}
    module_name_dict["switch"] = "libsan.switch"
    module_name_dict["array"] = "libsan.array"
    module_name_dict["physwitch"] = "libsan.physwitch"

    #Search for general entry on san config
    found_general_entry = False
    for entry in san_conf_list:
        if entry["name"] == "general":
            found_general_entry = True

    if not found_general_entry:
        general_entry = {}
        general_entry["name"] = "general"
        general_entry["options"] = {}
        san_conf_list.append(general_entry)

    for entry in san_conf_list:
        if entry["name"] == "general":
            entry["options"]["san_conf_file"] = san_conf


    #replace alias names by its value
    alias_dict = None
    for entry in san_conf_list:
        if entry["name"] == "alias":
            alias_dict = entry["options"]
    if alias_dict:
        for entry in san_conf_list:
            if entry["name"] == "alias":
                continue
            for sub_key in entry["options"].keys():
                if entry["options"][sub_key] in alias_dict.keys():
                    entry["options"][sub_key] = alias_dict[entry["options"][sub_key]]

    #We create a list of ports that are defined for this item
    for san_hw in san_conf_list:
        for key in san_hw["options"].keys():
            mac_key_regex = re.compile("mac_(.+)$")
            mac_match = mac_key_regex.match(key)
            if mac_match:
                mac = san_hw["options"][key]
                mac = net.standardize_mac(mac)
                if mac:
                    #The entry is a MAC or WWPN address
                    if "port_of" not in san_hw["options"].keys():
                        san_hw["options"]["port_of"] = {}
                    if mac in san_hw["options"]["port_of"].keys():
                        _print("FAIL: MAC %s is specific more then once on %s item section" % (mac, key))
                        print san_hw["options"]["port_of"]
                        return None
                    san_hw["options"]["port_of"][mac] = mac_match.group(1)
                    continue
                else:
                    _print("FAIL: Invalid MAC address %s on conf section %s" % (san_hw["options"][key], key))
                    return None

            wwpn_key_regex = re.compile("wwpn-(.+)$")
            wwpn_match = wwpn_key_regex.match(key)
            if wwpn_match:
                wwpn = san_hw["options"][key]
                wwpn = fc.standardize_wwpn(wwpn)
                if wwpn:
                    if "port_of" not in san_hw["options"].keys():
                        san_hw["options"]["port_of"] = {}
                    if wwpn in san_hw["options"]["port_of"].keys():
                        _print("FAIL: WWPN %s is specific more then once on %s item section" % (wwpn, key))
                        print san_hw["options"]["port_of"]
                        return None
                    san_hw["options"]["port_of"][wwpn] = wwpn_match.group(1)
                    continue
                else:
                    _print("FAIL: Invalid WWPN %s on conf section %s" % (san_hw["options"][key], key))
                    return None


#For iSCSI target
            value = san_hw["options"][key]
            if iscsi.is_iqn(value):
                if "port_of" not in san_hw["options"].keys():
                    san_hw["options"]["port_of"] = {}
                san_hw["options"]["port_of"][value] = key
                continue
#For physwitch
            simplex_key_regex = re.compile("^simplex_con_([^ ]+)$")
            simplex_match = simplex_key_regex.match(key)
            if simplex_match:
                if "simplex_con" not in san_hw["options"].keys():
                    san_hw["options"]["simplex_con"] = {}
                src_port = simplex_match.group(1)
                dst_port = san_hw["options"][key]
                san_hw["options"]["simplex_con"][src_port] = dst_port
                continue

            duplex_key_regex = re.compile("^duplex_con_([^ ]+)$")
            duplex_match = duplex_key_regex.match(key)
            if duplex_match:
                if "duplex_con" not in san_hw["options"].keys():
                    san_hw["options"]["duplex_con"] = {}
                src_port = duplex_match.group(1)
                dst_port = san_hw["options"][key]
                san_hw["options"]["duplex_con"][src_port] = dst_port
                san_hw["options"]["duplex_con"][dst_port] = src_port
                continue

    san_iscsi_conf = {}
    san_iscsi_conf["name"] = "iscsi_conf"
    san_iscsi_conf["hosts"] = {}
    for san_hw in san_conf_list:
        if "type" not in san_hw["options"].keys():
            continue
        if san_hw["options"]["type"] != "iscsi_initiator":
            if "model" not in san_hw["options"].keys():
                _print("FATAL: device %s does not have 'model' set on configuration" % san_hw["name"])
                return None
            #for example model: EMC-VNX should search class on emc.vnx
            model = san_hw["options"]["model"].lower()
            module = module_name_dict[san_hw["options"]["type"]]
            m = re.match("(\S+)-(\S+)", model)
            if not m:
                _print("FATAL: model for %s is not on right format (vendor-model)" % model)
                return None
            vendor = m.group(1)
            model = m.group(2)
            module += ".%s.%s" % (vendor, model)
            #The name of the class defined under vendor module should be named as the model
            class_name = "%s" % (model)
            san_hw["options"]["class_name"] = "%s" % (class_name)
            san_hw["options"]["module_name"] = "%s" % (module)
        else:
            #TODO iscsi_conf
            san_iscsi_conf["hosts"][san_hw["name"]] = {}
            iscsi_host_conf = san_iscsi_conf["hosts"][san_hw["name"]]
            for option in san_hw["options"].keys():
                m_iface = re.match("^iface_(\d+)$", option)
                if m_iface:
                    if not "ifaces" in iscsi_host_conf.keys():
                        san_iscsi_conf[san_hw["name"]]["ifaces"] = {}
                    if m_iface.group(1) not in iscsi_host_conf["ifaces"].keys():
                        iscsi_host_conf["ifaces"][m_iface.group(1)] = {}
                    iscsi_host_conf["ifaces"][m_iface.group(1)]["name"] = san_hw["options"][option]

                m_iface = re.match("^iface_disc_mod_(\d+)$", option)
                if m_iface:
                    if not "ifaces" in iscsi_host_conf.keys():
                        iscsi_host_conf["ifaces"] = {}
                    if m_iface.group(1) not in iscsi_host_conf["ifaces"].keys():
                        iscsi_host_conf["ifaces"][m_iface.group(1)] = {}
                    iscsi_host_conf["ifaces"][m_iface.group(1)]["disc_mod"] = san_hw["options"][option]

                m_iface = re.match("^iface_iqn_(\d+)$", option)
                if m_iface:
                    if not "ifaces" in iscsi_host_conf.keys():
                        iscsi_host_conf["ifaces"] = {}
                    if m_iface.group(1) not in iscsi_host_conf["ifaces"].keys():
                        iscsi_host_conf["ifaces"][m_iface.group(1)] = {}
                    iscsi_host_conf["ifaces"][m_iface.group(1)]["iqn"] = san_hw["options"][option]

                m_iface = re.match("^iface_ip_(\d+)$", option)
                if m_iface:
                    if not "ifaces" in iscsi_host_conf.keys():
                        iscsi_host_conf["ifaces"] = {}
                    if m_iface.group(1) not in iscsi_host_conf["ifaces"].keys():
                        iscsi_host_conf["ifaces"][m_iface.group(1)] = {}
                    iscsi_host_conf["ifaces"][m_iface.group(1)]["ip"] = san_hw["options"][option]


                m_iface = re.match("^iface_mac_(\d+)$", option)
                if m_iface:
                    if not "ifaces" in iscsi_host_conf.keys():
                        iscsi_host_conf["ifaces"] = {}
                    if m_iface.group(1) not in iscsi_host_conf["ifaces"].keys():
                        iscsi_host_conf["ifaces"][m_iface.group(1)] = {}
                    iscsi_host_conf["ifaces"][m_iface.group(1)]["mac"] = san_hw["options"][option]

                m_iface = re.match("^iface_trans_(\d+)$", option)
                if m_iface:
                    if not "ifaces" in iscsi_host_conf.keys():
                        iscsi_host_conf["ifaces"] = {}
                    if m_iface.group(1) not in iscsi_host_conf["ifaces"].keys():
                        iscsi_host_conf["ifaces"][m_iface.group(1)] = {}
                    iscsi_host_conf["ifaces"][m_iface.group(1)]["trans"] = san_hw["options"][option]

                m_iface = re.match("^target_ip_(\d+)$", option)
                if m_iface:
                    if not "ifaces" in iscsi_host_conf.keys():
                        iscsi_host_conf["ifaces"] = {}
                    if m_iface.group(1) not in iscsi_host_conf["ifaces"].keys():
                        iscsi_host_conf["ifaces"][m_iface.group(1)] = {}
                    iscsi_host_conf["ifaces"][m_iface.group(1)]["target_ip"] = san_hw["options"][option]


            iscsi_host_conf["hostname"] = san_hw["name"]
            #print san_conf_list["iscsi_conf"][entry]
            #if "iface" in san_conf_list[entry]["iscsi_conf"].keys()
            #    san_conf_list[entry]["iscsi_conf"]["iface"]
            continue
    san_conf_list.append(san_iscsi_conf)

    return san_conf_list

def check_sw_conf(config_file = None, switch_id = None):
    """
    Check if information from configuration file matches
    what is set on switch
    The arguments are:
    \tconfig_file         Full path to file where is the config
    \tswitch_id:          ID of an specific switch (optional)
    Returns:
    \tTrue:               All configuration is correct
    \tFalse:              There was a problem
    """
    config = _load_san_conf(config_file)

    if not config:
        _print("FAIL: Could not read %s" % config_file)
        return False

    total_error = 0
    #if switch_id paramter is not given we check all switches
    if switch_id:
        switch_ids = [switch_id]
    else:
        switch_ids = config.keys()

    for key in switch_ids:
        if "type" not in config[key]:
            #Entry does not have type, for example alias, general...
            continue
        if config[key]["type"] != "switch":
            #We are only interested on switches
            continue

        if "skip_check" in config[key].keys():
            if config[key]["skip_check"] == "1":
                print "INFO: skipping test on %s" % key
                #We do not want to check this switch
                continue

        if "model" not in config[key]:
            _print("FAIL: switch %s does not have model option" % key)
            return False

        if "ctrl_ip" not in config[key]:
            _print("FAIL: switch %s does not ctrl_ip configured" % key)
            return False
        if "ctrl_user" not in config[key]:
            _print("FAIL: switch %s does not have ctrl_user configured" % key)
            return False
        if "ctrl_pass" not in config[key]:
            _print("FAIL: switch %s does not have ctrl_pass configured" % key)
            return False

        #We only support ssh
        if "ctrl_type" not in config[key]:
            _print("FAIL: switch %s does not have ctrl_type configured" % key)
            return False
        if "ssh" not in config[key]["ctrl_type"]:
            _print("FAIL: we do not support ctrl_type %s on %s" % (config[key]["ctrl_type"]), key)
            return False

        ip      = config[key]["ctrl_ip"]
        user    = config[key]["ctrl_user"]
        passwd  = config[key]["ctrl_pass"]

        switch = None

        #we currently only support Cisco NXOS
        if config[key]["model"] == "Cisco-NXOS":
            switch = nxos.nxos(ip, user, passwd)
        elif config[key]["model"] == "Brocade-FOS":
            switch = fos.fos(ip, user, passwd)
        else:
            #unsupported switch
            continue
        _print("INFO: Checking configuration for switch %s (%s)" % (key, ip))
        switch_error = 0

        wwpn_dict = switch.wwpn_2_port_id()
        if not wwpn_dict:
            #Did not find any WWPN entry
            continue
        #Let's check if the ports that are online on the switch match what we have on config
        for p_id in wwpn_dict.keys():
            alias = get_alias(config, wwpn_dict[p_id])
            if p_id not in config[key].keys():
                if alias:
                    print("WARN: Port '%s' is not defined on '%s' on file '%s' on switch has wwpn %s (%s)"
                       % (p_id, key, config_file, alias, wwpn_dict[p_id]))
                else:
                    print("WARN: Port '%s' is not defined on '%s' on file '%s' on switch has wwpn %s"
                       % (p_id, key, config_file, wwpn_dict[p_id]))
                continue
            #need to check if the wwpn on the switch port matches what we expect on config file
            if wwpn_dict[p_id] != config[key][p_id]:
                alias_cfg = get_alias(config, config[key][p_id])
                if alias:
                    print("FAIL: switch %s port '%s' has wwpn '%s (%s)', but on config it expected to be '%s (%s)'"
                       % (key, p_id, alias, wwpn_dict[p_id], alias_cfg, config[key][p_id]))
                else:
                    print("FAIL: switch %s port '%s' has wwpn '%s', but on config it expected to be '%s'"
                       % (key, p_id, wwpn_dict[p_id], config[key][p_id]))
                switch_error += 1

        cap_dict = switch.capability()
        if cap_dict["eth_switch"]:
            mac_dict = switch.mac_2_port_id()
            if not mac_dict:
                #Did not find any MAC entry
                continue
            #Let's check if the ports that are online on the switch match what we have on config
            for p_id in mac_dict.keys():
                if p_id not in config[key].keys():
                    #Not sure when we use MAC, so do not need to give any warnings
                    #_print("WARN: Port '%s' is not defined on '%s' on file '%s' on switch has mac %s"
                    #    % (p_id, key, config_file, mac_dict[p_id]))
                    continue
                #need to check if the wwpn on the switch port matches what we expect on config file
                if mac_dict[p_id] != config[key][p_id]:
                    _print("FAIL: switch port '%s' has mac '%s', but on config it expected to be '%s'" % (p_id, mac_dict[p_id], config[key][p_id]))
                    switch_error += 1
        if not switch_error:
            print "PASS: No problem found on %s" % key
        total_error += switch_error
    if total_error:
        #_print("FAIL: Check switch configuration")
        return False
    #_print("PASS: Check switch configuration")
    return True


def choose_mpaths(mpath_name=None, exclude_boot_device=True, exclude_lvm_device=True):
    """
    choose_mpaths ()
    Usage
        choose_mpaths($mpath_name)
    Purpose
        If mpath_name defined, we just choose it only.
        Return $ref_choosed_mpath. Check the head of this pod for detailed data
        structure.
        We will choose all SAN mpath devices. If more then one device has
        same Vendor/Product, t_wwpn/h_wwpn or t_inq/h_iqn we choose only one of them
            t_wwpns, h_wwpns, single storage array.
        To choose mpath, we follow this rule:
            1. Each vendor/product.
            1. Each HBA. (h_wwpn)
            1. Each storage array even they are same vendor/product.
    Parameter
        mpath_name (optional)                   if given and mpath_name exist, return info for this mpath
        exclude_boot_device (default = True)    Do not return mpath that is used for boot
        exclude_lvm_device  (default = True)    Do not return any mpath that is used as LVM PV
    Returns
        ref_choosed_mpath
    """

    choosed_mp_infos = []
    choosed_mpath_dict = {}

    free_mpaths = mp.get_free_mpaths(exclude_boot_device=exclude_boot_device, exclude_lvm_device=exclude_lvm_device)
    if not free_mpaths:
        return None

    #If mpath name is given and it exists return it
    #If given mpath does not exist, return None
    if mpath_name:
        if mpath_name in free_mpaths.keys():
            choosed_mpath_dict[mpath_name] = free_mpaths[mpath_name]
            return choosed_mpath_dict
        return None


    for mp_info in free_mpaths.values():
        if not mp_info:
            continue

        #Add the first multipath device to the choosen list
        if not choosed_mp_infos:
            choosed_mp_infos.append(mp_info)
            continue

        #check vendor/product
        if "vendor" not in mp_info.keys() or "product" not in mp_info.keys():
            _print("WARN: %s has not vendor or product info. Skipping." % mp_info["mpath_name"])
            continue

        if (not mp_info["h_wwpns"] and not mp_info["t_wwpns"] and not
              mp_info["h_iqns"] and not mp_info["t_iqns"]):
            _print("WARN: %s has not seem to be SAN device. Skipping." % mp_info["mpath_name"])
            continue

        #Flag to skip is info already exists on choosed_mp_infos
        found_info = False

        #If there is a device with same info, no need to add another one
        for choosed_mp_info in choosed_mp_infos:
            #_print("DEBUG: mp vendor: [%s] c_mp vendor: [%s]" % (mp_info["vendor"], choosed_mp_info["vendor"]))
            #_print("DEBUG: mp product: [%s] c_mp product: [%s]" % (mp_info["product"], choosed_mp_info["product"]))
            #_print("DEBUG: mp h_iqns: [%s] c_mp h_iqns: [%s]" % (",".join(mp_info["h_iqns"]), ",".join(choosed_mp_info["h_iqns"])))
            if (mp_info["vendor"] == choosed_mp_info["vendor"] and
                mp_info["product"] == choosed_mp_info["product"] and
                array.is_array_same(mp_info["h_wwpns"], choosed_mp_info["h_wwpns"]) and
                array.is_array_same(mp_info["t_wwpns"], choosed_mp_info["t_wwpns"]) and
                #Equalogic has 1 target for each LUN, so if host has more then 1 LUN it will have more then 1 t_iqn
                #array.is_array_same(mp_info["h_iqns"], choosed_mp_info["h_iqns"]) and
                #array.is_array_same(mp_info["t_iqns"], choosed_mp_info["t_iqns"])):
                array.is_array_same(mp_info["h_iqns"], choosed_mp_info["h_iqns"])):
                found_info = True
                continue
        if not found_info:
            #If info from current mp_info is not included on choosed_mp_infos. we add it
            choosed_mp_infos.append(mp_info)

    if not choosed_mp_infos:
        _print("FAIL: choose_mpath(): no mpath choosed for testing")

    for choosed_mp_info in choosed_mp_infos:
        if "mpath_name" in choosed_mp_info.keys():
            choosed_mpath_dict[choosed_mp_info["mpath_name"]] = choosed_mp_info

    return choosed_mpath_dict

def setup_iscsi():
    obj_sanmgmt = SanMgmt()
    return obj_sanmgmt.setup_iscsi()

def _get_class(module, class_name):
    """
    Get a class dynamically
    """
    #importlib is not available on python 2.6 (RHEL-6)
    #import importlib
    #MyClass = getattr(importlib.import_module(module), class_name)
    #return MyClass
    try:
        mod = __import__("%s" % module, fromlist=[class_name])
        MyClass = getattr(mod, class_name)
        return MyClass
    except:
        return None

class SanMgmt:
    """
    obj = SanMgmt()

    obj.h_wwpns( 'st05_01', '10:00:00:00:c9:95:2f:de' )
    obj.t_wwpns('netapp', '50:0a:09:85:99:4b:8d:c5')
    obj.macs( 'st51_nic_01', '08:00:27:05:37:71' )

    obj.load_conf("/etc/san_top.conf")

    # FC/FCoE/Ethernet Switch
    obj.link_trigger( action => "UP", addr => 'st51_01' )
    obj.link_trigger( action => "DOWN", addr => 'st51_01' )
    obj.link_all_up()


    # Storage Array
    lun_name = obj.lun_create("1GiB")
    ok       = obj.lun_map($lun_name)
    lun_name = obj.lun_create_and_map("1GiB")
    ok       = obj.lun_unmap($lun_name)
    ok       = obj.lun_unmap_regex($regex)
    ok       = obj.lun_remove($lun_name)
    new_lun_size_bytest = obj.lun_grow(
                                lun_name => lun_name,
                                increment => "1GiB" )
    new_lun_size_bytest = obj->lun_shrink(
                                lun_name => lun_name,
                                increment => "1GiB" )
    lun_size_bytes  = obj.lun_size_of($lun_name)
    lun_size_human  = obj.lun_size_of(lun_name=>$lun_name, human=>1)
    ref_scsi_info   = obj.query_scsi_info( recheck => 1 )
    ref_my_lun_info = obj.query_my_lun_info( recheck => 1 ) )

    # Layer 1/Physical Switch
    ok = obj.port_disconnect("st05_01")
    ok = obj.port_connect("st05_01")
    ok = obj.port_flap(
        addr      => "st05_02",
        up_time   => "100",
        down_time => "100",
        count     => "100"
      )


    # Misc
    ok = obj.rescan_host()
    """

    san_conf_list = []       #Store current object settings
    _h_iqns = []
    _t_iqns = []
    _h_wwpns = []
    _t_wwpns = []
    _macs = []
    _addrs = []
    _map_info = []


    def load_conf(self, san_conf = None):

        self.san_conf_list = _load_san_conf(san_conf)
        if not self.san_conf_list:
            self.san_conf_list = []
            san_general_dict = {}
            san_general_dict["conf_loaded"] = False
            self.san_conf_list.append(san_general_dict)
            _print("FAIL: Could not load san config")
            return False

        #self.san_conf_list["san_conf_file"] = san_conf
#        if "alias" in san_conf_list.keys():
#            self.san_conf_list["alias"] = san_conf_list["alias"]

        # expand alias in h_wwpns, t_wwpns, h_iqns, t_iqns, macs, addrs
        tmp_loop_items = ["t_wwpns", "h_wwpns", "t_iqns", "macs", "addrs"]
        #if self.san_conf_list["alias"]:
            #_print("DEBUG: expand alias")
            #print self.san_conf_list["alias"]
            #for item in tmp_loop_items:
                #if item in self.san_conf_list["alias"].keys():
                    #print self.san_conf_list["alias"][item]

        self._load_sa_conf()

        addrs = []
        if self._addrs:
            addrs.extend(self._addrs)
        if self._macs:
            addrs.extend(self._macs)
        if self._h_wwpns:
            addrs.extend(self._h_wwpns)
        if self._t_wwpns:
            addrs.extend(self._t_wwpns)
        if self._t_iqns:
            addrs.extend(self._t_iqns)

        self._addrs = array.dedup(addrs)

        #load switch config uses _addrs
        self._load_sw_conf()
        self._load_sw_conf(flag_physwitch="physwitch")

        for san_hw in self.san_conf_list:
            if san_hw["name"] == "general":
                san_general_dict = san_hw["options"]
                san_general_dict["conf_loaded"] = True

        return True

    def is_config_loaded(self):
        """
        """
        for san_hw in self.san_conf_list:
            if san_hw["name"] == "general":
                san_general_dict = san_hw["options"]
                if "conf_loaded" in san_general_dict.keys():
                    return san_general_dict["conf_loaded"]

        return False

    def _load_sa_conf(self):
        """
        Usage
            self->_load_sa_conf(san_conf)
        Purpose
            Load configuration file for storage array settings.
            We setup self.san_conf_list["sa_conf"] base on WWPN/IQN and san_conf
            This method should not generate any output.
            check_conf() will warn user for any incorrect confure.
        Parameter
            san_conf           # the return reference of san_conf
        Returns
            True
                or
            False
        """
        san_conf_list = self.san_conf_list

        if not san_conf_list:
            return False
        sa_conf_dict = {}
        sa_conf_dict["name"] = "sa_conf"
        sa_conf_dict["devices"] = {}
        san_general_dict = {}
        #search for general configuration
        for san_hw in san_conf_list:
            if san_hw["name"] == "general":
                san_general_dict = san_hw["options"]

        san_capability_dict = {}
        found_capability = False
        #search for capability configuration
        for san_hw in san_conf_list:
            if san_hw["name"] == "capability":
                san_capability_dict = san_hw
                found_capability     = True
        if not found_capability:
            san_capability_dict["name"] = "capability"
            san_capability_dict["options"] = {}
            san_conf_list.append(san_capability_dict)

        _targets = []
        if self._t_iqns:
            _targets.extend(self._t_iqns)
        if self._t_wwpns:
            _targets.extend(self._t_wwpns)

        _all_configured_tgt_ports = []

        for san_hw in san_conf_list:
            san_dev = san_hw["name"]
            if "options" not in san_hw.keys():
                continue
            dev_conf_dict = san_hw["options"]
            if "type" not in dev_conf_dict.keys():
                continue
            if dev_conf_dict["type"] != "array":
                continue
            #Check if array has port configured
            if "port_of" not in dev_conf_dict.keys():
                continue

            _all_configured_tgt_ports.extend(dev_conf_dict["port_of"])

            if _targets:
                #We are connected to some target
                for target in _targets:
                    if target in dev_conf_dict["port_of"].keys():
                        #Add SAN device to sa_config dict
                        if ("devices" in sa_conf_dict.keys() and
                               san_dev in sa_conf_dict["devices"].keys()):
                            #We already stored the configuration of this device
                            continue
                        if san_dev not in sa_conf_dict.keys():
                            if "ctrl_ip" not in dev_conf_dict.keys():
                                _print("FAIL: _load_sa_conf() - ctrl_ip not set on %s" % san_dev)
                                continue
                            ip = dev_conf_dict["ctrl_ip"]

                            if "ctrl_user" not in dev_conf_dict.keys():
                                _print("FAIL: _load_sa_conf() - ctrl_user not set on %s" % san_dev)
                                continue
                            user = dev_conf_dict["ctrl_user"]

                            if "ctrl_pass" not in dev_conf_dict.keys():
                                _print("FAIL: _load_sa_conf() - ctrl_pass not set on %s" % san_dev)
                                continue
                            passwd = dev_conf_dict["ctrl_pass"]

                            tmo = None
                            if "ctrl_tmo" in dev_conf_dict.keys():
                                tmo = dev_conf_dict["ctrl_tmo"]
                            #Initializing SAN class, the class_obj was set on _load_san_conf
                            array_class = _get_class(dev_conf_dict["module_name"],
                                                     dev_conf_dict["class_name"])
                            if array_class:
                                #san_dev parameter is needed for Equalogic to know to which device it should add
                                #the new target on lun_create
                                array_obj = array_class(ip, user, passwd, timeout=tmo, san_dev=san_dev)
                                if array_obj:
                                    array_obj.set_san_conf_path(san_general_dict["san_conf_file"])
                                    array_obj.set_sa_conf(dev_conf_dict)
                                    dev_conf_dict["class_obj"] = array_obj
                                    #_print("DEBUG: load_conf: %s uses class: %s.%s" %
                                    #       (san_dev,
                                    #        dev_conf_dict["module_name"],
                                    #        dev_conf_dict["class_name"]))
                                    san_capability_dict["options"].update(array_obj.capability())
                                else:
                                    _print("FAIL: %s could not initialize class %s.%s" %
                                           (san_dev,
                                            dev_conf_dict["module_name"],
                                            dev_conf_dict["class_name"]))
                            else:
                                _print("FAIL: could not load class %s.%s" %
                                       (dev_conf_dict["module_name"],
                                        dev_conf_dict["class_name"]))

                            found_sa_conf = False
                            for san_hw in san_conf_list:
                                if san_hw["name"] == "sa_conf":
                                    found_sa_conf = True
                            if not found_sa_conf:
                                san_conf_list.append(sa_conf_dict)

                            sa_conf_dict["devices"][san_dev] = dev_conf_dict

        #remove target ports that are not configure as valid target port on san_top.conf file
        if self._t_iqns:
            #Replace t_wwpn list with a new list with only configured member
            self._t_iqns = [t_iqn for t_iqn in self._t_iqns if _all_configured_tgt_ports and t_iqn in _all_configured_tgt_ports]
            #for t_iqn in self._t_iqns:
            #    if not _all_configured_tgt_ports or t_iqn not in _all_configured_tgt_ports:
            #        self._t_iqns.remove(t_iqn)
        if self._t_wwpns:
            #Replace t_wwpn list with a new list with only configured member
            self._t_wwpns = [t_wwpn for t_wwpn in self._t_wwpns if _all_configured_tgt_ports and t_wwpn in _all_configured_tgt_ports]
            #for t_wwpn in self._t_wwpns:
            #    if not _all_configured_tgt_ports or t_wwpn not in _all_configured_tgt_ports:
            #        self._t_wwpns.remove(t_wwpn)

        found_sa_conf = False
        for san_hw in san_conf_list:
            if san_hw["name"] == "sa_conf":
                found_sa_conf = True
        if not found_sa_conf:
            return False

        return True

    def _load_sw_conf(self, flag_physwitch=None):
        """
        Usage
            self->_load_sw_conf(san_conf)
        Purpose
            Load configuration file for storage switch settings.
            We setup self.san_conf_list["sw_conf"] base on WWPN/IQN and san_conf
            This method should not generate any output.
            check_conf() will warn user for any incorrect confure.
        Parameter
            san_conf           # the return reference of san_conf
        Returns
            True
                or
            False
        """
        san_conf_list = self.san_conf_list
        if not san_conf_list:
            return False
        switch_type = "switch"
        obj_key = "sw_conf"
        if flag_physwitch and flag_physwitch == "physwitch":
            #It is a physical layer switch
            switch_type = "physwitch"
            obj_key = "physw_conf"

        sws_conf_dict = {}
        sws_conf_dict["name"] = obj_key
        sws_conf_dict["devices"] = {}

        san_capability_dict = {}
        found_capability = False
        #search for capability configuration
        for san_hw in san_conf_list:
            if san_hw["name"] == "capability":
                san_capability_dict = san_hw
                found_capability     = True
        if not found_capability:
            san_capability_dict["name"] = "capability"
            san_capability_dict["options"] = {}
            san_conf_list.append(san_capability_dict)

        for san_hw in san_conf_list:
            san_dev = san_hw["name"]
            if "options" not in san_hw.keys():
                continue
            dev_conf_dict = san_hw["options"]
            if "type" not in dev_conf_dict.keys():
                continue
            if dev_conf_dict["type"] != switch_type:
                continue
            #Check if array has port configured
            if "port_of" not in dev_conf_dict.keys():
                continue
            #Need to process each port individually as they might have
            #different capability, like FC or eth ports
            for addr in dev_conf_dict["port_of"].keys():
                #In case there are duplicated address
                #do not process it more than once
                if addr in sws_conf_dict.keys():
                    continue
                ip = dev_conf_dict["ctrl_ip"]
                user = dev_conf_dict["ctrl_user"]
                passwd = dev_conf_dict["ctrl_pass"]
                #Initializing SAN class, the class_obj was set on _load_san_conf
                switch_class = _get_class(dev_conf_dict["module_name"],
                                            dev_conf_dict["class_name"])
                if switch_class:
                    #_print("DEBUG: load config switch %s %s %s" % (ip, user, passwd))
                    switch_obj = switch_class(ip, user, passwd)
                    if switch_obj:
                        dev_conf_dict["class_obj"] = switch_obj
                        #_print("DEBUG: load_conf: %s uses class: %s.%s" %
                        #        (san_dev,
                        #        dev_conf_dict["module_name"],
                        #        dev_conf_dict["class_name"]))
                        #san_capability_dict["options"].update(switch_obj.capability())
                    else:
                        _print("FAIL: %s could not initialize class %s.%s" %
                                (san_dev,
                                dev_conf_dict["module_name"],
                                dev_conf_dict["class_name"]))
                else:
                    _print("FAIL: could not load class %s.%s" %
                            (dev_conf_dict["module_name"],
                            dev_conf_dict["class_name"]))
                    continue

                #Switch capability present in all switches
                all_sw_caps = ["link_down", "link_up", "switch_reboot",
                                "phy_port_disconnect", "phy_port_connect",
                                "phy_port_flap", "phy_switch_reboot"]
                if switch_obj:
                    #We only add the info if switch has the capability
                    switch_cap = switch_obj.capability()
                    for sw_cap in all_sw_caps:
                        if sw_cap in switch_cap.keys() and switch_cap[sw_cap]:
                            if sw_cap not in san_capability_dict["options"].keys():
                                san_capability_dict["options"][sw_cap] = []
                            san_capability_dict["options"][sw_cap].append(addr)

                if fc.is_wwn(addr) and switch_obj:
                    fc_sw_caps = ["fc_switch", "fc_physwitch"]
                    switch_cap = switch_obj.capability()
                    for sw_cap in fc_sw_caps:
                        #We only add the info if switch has the capability
                        if sw_cap in switch_cap.keys() and switch_cap[sw_cap]:
                            if sw_cap not in san_capability_dict["options"].keys():
                                san_capability_dict["options"][sw_cap] = []
                            san_capability_dict["options"][sw_cap].append(addr)
                    #TODO if is eth switch


                found_obj_conf = False
                for san_hw in san_conf_list:
                    if san_hw["name"] == obj_key:
                        found_obj_conf = True
                if not found_obj_conf:
                    san_conf_list.append(sws_conf_dict)

                sws_conf_dict["devices"][san_dev] = dev_conf_dict

        found_obj_conf = False
        for san_hw in san_conf_list:
            if san_hw["name"] == obj_key:
                found_obj_conf = True
        if not found_obj_conf:
            return False

        return True


    def check_ports_ready(self):
        """
        Check if all ports that we will need for the test are UP on switch
        """
        _print("INFO: Checking if all ports that we need are UP...")
        h_wwpns = fc.h_wwpn_of_host()
        t_wwpns = fc.t_wwpn_of_host()

        if h_wwpns:
            for h_wwpn in h_wwpns:
                sw = self.get_sw_self(h_wwpn)
                if sw:
                    _print("INFO: checking port %s..." % h_wwpn)
                    if self.port_state(h_wwpn) != "UP":
                        #Server port is not up
                        #We can try to bring it UP
                        self.link_trigger(action="UP", wwpn=h_wwpn)
                        if self.port_state(h_wwpn) != "UP":
                            _print("FAIL: We are not able to bring port %s UP" % h_wwpn)
                            return False
                    #If switch supports enable RCSN messages we want to make sure it is enabled
                    capability = self.get_sw_capability(h_wwpn)
                    if capability:
                        if "rcsn_enable" in capability and capability["rcsn_enable"]:
                            if not self.rcsn_trigger(action="ENABLE", wwpn=h_wwpn):
                                _print("FAIL: We are not able to enable RCSN on port %s" % h_wwpn)
                                return False
                else:
                    #port is not managed by us
                    continue
        if t_wwpns:
            for t_wwpn in t_wwpns:
                sw = self.get_sw_self(t_wwpn)
                if sw:
                    _print("INFO: checking port %s..." % t_wwpn)
                    if self.port_state(t_wwpn) != "UP":
                        _print("FAIL: Target port %s is NOT UP, maybe some other test brought it DOWN on purpose" % t_wwpn)
                        return False
                        #If switch supports enable RCSN messages we want to make sure it is enabled
                    capability = self.get_sw_capability(t_wwpn)
                    if capability:
                        if "rcsn_enable" in capability and capability["rcsn_enable"]:
                            if not self.rcsn_trigger(action="ENABLE", wwpn=t_wwpn):
                                _print("FAIL: We are not able to enable RCSN on port %s" % t_wwpn)
                                return False
                else:
                    #port is not managed by us
                    continue

        nics = net.get_nics()
        if nics:
            for nic in nics:
                if nic == "lo":
                    #skip loopback interface
                    continue
                mac = net.get_mac_of_nic(nic)
                if not mac:
                    _print("FAIL: Could not get MAC for interface %s" % nic)
                    continue
                sw = self.get_sw_self(mac)
                if sw:
                    _print("INFO: checking port %s..." % mac)
                    if self.port_state(mac) != "UP":
                        #Server port is not up
                        #We can try to bring it UP
                        self.link_trigger(action="link_up", addr=mac)
                        if self.port_state(mac) != "UP":
                            _print("FAIL: We are not able to bring port %s UP" % mac)
                            return False
                    #If switch supports enable RCSN messages we want to make sure it is enabled
                    capability = self.get_sw_capability(mac)
                    if capability:
                        if "rcsn_enable" in capability and capability["rcsn_enable"]:
                            if not self.rcsn_trigger(action="ENABLE", wwpn=h_wwpn):
                                _print("FAIL: We are not able to enable RCSN on port %s" % h_wwpn)
                                return False
                else:
                    #port is not managed by us
                    continue
        #TODO iSCSI Target MAC address

        _print("INFO: PASS all ports that we need are UP.")
        return True

    def h_wwpns(self, h_wwpns = None):
        """
        Usage
            obj.h_wwpns()              #For querying
            obj.h_wwpns(@h_wwpns)      #For setting
        Purpose
            Setup Host HBA WWPNs. Only useful you want to control remote
            server's FC SAN. If localhost is the server we are working, we will
            find out the Host HBA WWPNs via libsan.host.fc.
        Parameters
            @h_wwpns
        Returns
            List: h_wwpns
                or
            None
        """
        #We are going to set h_wwpns
        if (h_wwpns):
            self._h_wwpns = h_wwpns

        #Does not matter if we set the list or just and to read it, we always return the list
        return self._h_wwpns

    def t_wwpns(self, t_wwpns = None):
        """
        Usage
            obj.t_wwpns()              #For querying
            obj.t_wwpns(@t_wwpns)      #For setting
        Purpose
            Setup Setup Storage Array/Target WWPNs. Only useful you want to control remote
            server's FC SAN. If localhost is the server we are working, we will
            find out the Host HBA WWPNs via libsan.host.fc.
        Parameters
            @t_wwpns
        Returns
            List: t_wwpns
                or
            None
        """
        #We are going to set h_wwpns
        if (t_wwpns):
            self._t_wwpns = t_wwpns

        #Does not matter if we set the list or just and to read it, we always return the list
        return self._t_wwpns

    def h_iqns(self, h_iqns = None):
        """
        Usage
            obj.h_iqns()              #For querying
            obj.h_iqns(h_iqns)        #For setting
        Purpose

        Parameters
            @t_wwpns
        Returns
            List: h_iqns
                or
            None
        """
        #We are going to set h_wwpns
        if (h_iqns):
            self._h_iqns = h_iqns

        #Does not matter if we set the list or just and to read it, we always return the list
        return self._h_iqns

    def t_iqns(self, t_iqns = None):
        """
        Usage
            obj.t_iqns()              #For querying
            obj.t_iqns(t_iqns)        #For setting
        Purpose

        Parameters
            t_iqns
        Returns
            List: t_iqns
                or
            None
        """
        #We are going to set t_iqns
        if (t_iqns):
            self._t_iqns = t_iqns

        #Does not matter if we set the list or just and to read it, we always return the list
        return self._t_iqns

    def macs(self, macs = None):
        """
        Usage
            obj.macs()                #For querying
            obj.macs(macs)      #For setting
        Purpose

        Parameters
            macs
        Returns
            List: macs
                or
            None
        """
        #We are going to set t_iqns
        if (macs):
            self._macs = macs

        #Does not matter if we set the list or just and to read it, we always return the list
        return self._macs

    def map_info(self, map_info=None):
        """
        Usage
            obj.map_info()                    #For querying
            obj.map_info(map_info_list)       #For setting
        Purpose

        Parameters
            list with map info
        Returns
            List: map_info_list
                or
            None
        """
        #We are going to set t_iqns
        if (map_info):
            self._map_info = map_info

        #Does not matter if we set the list or just and to read it, we always return the list
        return self._map_info

    def capability(self):
        san_capability_dict = {}
        found_capability = False
        #search for capability configuration
        for san_hw in self.san_conf_list:
            if san_hw["name"] == "capability":
                san_capability_dict = san_hw["options"]
                found_capability     = True
        if not found_capability:
            san_capability_dict["name"] = "capability"
            san_capability_dict["options"] = {}
            self.san_conf_list.append(san_capability_dict)

        return san_capability_dict

    def sa_names(self):
        """
        Usage
            obj->sa_name()
        Purpose
            Query out the name for the storage array.
        Parameter
            N/A
        Returns
            List:   Storage Array names, most of the cases it will have just 1 element
        """
        for san_hw in self.san_conf_list:
            if san_hw["name"] == "sa_conf":
                return san_hw["devices"].keys()
        return None


    def sw_names(self):
        """
        Usage
            obj->sw_name()
        Purpose
            Query out the name for the storage switch.
        Parameter
            N/A
        Returns
            List:   Storage switch names
        """
        for san_hw in self.san_conf_list:
            if san_hw["name"] == "sw_conf":
                return san_hw["devices"].keys()
        return None

    def physw_names(self):
        """
        Usage
            obj->physw_name()
        Purpose
            Query out the name for the apcon storage switch.
        Parameter
            N/A
        Returns
            List:   Storage switch names
        """
        for san_hw in self.san_conf_list:
            if san_hw["name"] == "physw_conf":
                return san_hw["devices"].keys()
        return None


    def get_sa_self(self):
        """
        Return the Storage Array configuration for this obj
        Return None if there is more than 1 array configured
        """

        sa_self = None
        for san_hw in self.san_conf_list:
            if san_hw["name"] == "sa_conf":
                sa_self = san_hw["devices"]
        if not sa_self:
                print "FAIL: This server has no storage array configured for it"
                return None
        if len(sa_self.keys()) > 1:
            _print("FAIL: get_sa_self - san config has more then 1 array")
            print sa_self.keys()
            return None
        for key in sa_self.keys():
            #we know there is only 1 entry. we return it
            return sa_self[key]

    def get_sa_obj_self(self):
        sa_val = self.get_sa_self()
        if not sa_val:
            _print("FAIL: get_sa_obj_self(): Could not find an storage array to use")
            return None

        if "class_obj" not in sa_val.keys():
            _print("FAIL: get_sa_obj_self() - SanMgmt did not load Storage class")
            return None

        sa_obj = sa_val["class_obj"]
        return sa_obj

    def get_sw_self(self, port):
        """
        Return the Switch that has 'port' connected to it
        """
        switches_dict = None
        for san_hw in self.san_conf_list:
            if san_hw["name"] == "sw_conf":
                switches_dict = san_hw["devices"]
        if not switches_dict:
                return None
        if not port:
            _print("FAIL: get_sw_self() - requires port parameter")
            return None

        orig_port = port
        port = self._addr_of(port)
        if not port:
            _print("FAIL: get_sw_self() - %s is an invalid WWPN/MAC/IQN" % orig_port)
            return None

        for sw in switches_dict.keys():
            if "port_of" in switches_dict[sw].keys() and port in switches_dict[sw]["port_of"].keys():
                return switches_dict[sw]
        return None

    def get_sw_capability(self, port):
        """
        Return the switch capability of the switch connected to given port
        """

        #Get the switch connected to this port
        connected_sw = self.get_sw_self(port)
        if not connected_sw:
            _print("FAIL: get_sw_capability(): No switch in configuration is "
                + "controlling %s, even though it pass config check..." % port)
            return False

        sw_obj = connected_sw["class_obj"]

        return sw_obj.capability()

    def get_physw_self(self, port):
        """
        Return the physical Switch that has 'port' connected to it
        """
        switches_dict = None
        for san_hw in self.san_conf_list:
            if san_hw["name"] == "physw_conf":
                switches_dict = san_hw["devices"]
        if not switches_dict:
                return None
        if not port:
            _print("FAIL: get_physw_self() - requires port parameter")
            return None

        for sw in switches_dict.keys():
            if "port_of" in switches_dict[sw].keys() and port in switches_dict[sw]["port_of"].keys():
                return switches_dict[sw]
        return None

    def wwid_of_lun(self, lun_name):
        """
        Usage
            obj->wwid_of_lun(lun_name)
        Purpose
            Find out the WWID (vpd 0x83) of certain LUN.
            Some storage array(eg. LIO) might not provide this info.
        Parameter
            lun_name       # LUN name
        Returns
            wwid
        """
        if not lun_name:
            _print("FAIL: wwid_of_lun() - requires lun_name parameter")
            return None

        lun_info = self.lun_info(lun_name)
        if lun_info:
            if "wwid" in lun_info.keys():
                return lun_info["wwid"]
        _print("INFO: wwid_of_lun() - LUN %s has no WWID" % lun_name)
        print lun_info
        return None


    def scsi_id_of_lun(self, lun_name=None, h_wwpn=None, t_wwpn=None, h_iqn=None, t_iqn=None):
        """
        Usage
            obj.scsi_id_of_lun(lun_name=lun_name)
            obj.scsi_id_of_lun(lun_name=lun_name, h_wwpn=h_wwpn,
                t_wwpn=>$t_wwpn)
        #we should not support this method since the qla4xxx iqn issue
        #    obj.scsi_id_of_lun(lun_name=lun_name, h_iqn=h_iqn,
                t_iqn=t_iqn)
            obj.scsi_id_of_lun(lun_name=lun_name, iface=iface,
                t_iqn=t_iqn, target_ip=target_ip)   # TODO
        Purpose
            To uniquly identify a SCSI ID, we can use these info:
            1. h_wwpn, t_wwpn, wwid  # wwid can be found via lun_name
            2. h_wwpn, t_wwpn, lun_id    # lun_id can be found via lun_name
            3. t_iqn, target_ip, iface   # TODO
            If less info provided in parameters, we will return all SCSI ID matches.
            Some storage array does not provide wwid of certain LUN, if so, we use
            lun_id, h_wwpn, t_wwpn to identify SCSI ID out.
        Parameter
            lun_name               # then name used inside of storage array
            h_wwpn                 # like '10:00:00:00:c9:95:2f:df'
            t_wwpn                 # like '50:0a:09:86:99:4b:8d:c5'
            h_iqn                  # the IQN used by iscsi initiator
            t_iqn                  # the IQN used by iscsi target
        Returns
            scsi_ids
                or
            None
        """
        if not lun_name:
            _print("FAIL: scsi_id_of_lun() - requires lun_name parameter")
            return False
        h_wwpns = []
        t_wwpns = []
        h_iqns = []
        t_iqns = []

        if h_wwpn:
            h_wwpn = fc.standardize_wwpn(h_wwpn)
            if h_wwpn:
                h_wwpns.append(h_wwpn)
        else:
            wwpns = self.h_wwpns()
            if wwpns:
                h_wwpns = wwpns

        if t_wwpn:
            t_wwpn = fc.standardize_wwpn(t_wwpn)
            if t_wwpn:
                t_wwpns.append(t_wwpn)
        else:
            wwpns = self.t_wwpns()
            if wwpns:
                t_wwpns = wwpns

        if h_iqn:
                h_iqns.append(h_iqn)
        else:
            iqns = self.h_iqns()
            if iqns:
                h_iqns = iqns

        if t_iqn:
                t_iqns.append(t_iqn)
        else:
            iqns = self.t_iqns()
            if iqns:
                t_iqns.append(iqns)

        wwid = self.wwid_of_lun(lun_name)

        lun_scsi_ids = None
        found_scsi_ids = None
        if wwid:
            _print("INFO: LUN %s has WWID %s" % (lun_name, wwid))
            lun_scsi_ids = scsi.scsi_ids_of_wwid(wwid)

        #Not sure why this code is needed
        #for h_wwpn in h_wwpns:
            #zoned_t_wwpns = fc.t_wwpn_of(h_wwpn)
            #for t_wwpn in t_wwpns:
                #if t_wwpn not in zoned_t_wwpns:
                    #continue
                #target_id = fc.fc_target_id_of_htwwpn(t_wwpn = t_wwpn, h_wwpn = h_wwpn)
                #_print("FATAL: scsi_id_of_lun - TODO")
        #_print("DEBUG: LUN %s has the following scsi ids" % lun_name)
        #print lun_scsi_ids
        #for h_iqn in h_iqns:
            #for t_iqn in t_iqns:
                #_print("DEBUG: searching for scsi_ids from iSCSI session with %s %s" % (h_iqn, t_iqn))
                #ses_scsi_ids = iscsi.scsi_ids_of_iscsi_session(h_iqn = h_iqn, t_iqn = t_iqn)
                #if not ses_scsi_ids:
                    #continue
                #_print("DEBUG: found scsi_ids from iSCSI session with %s %s" % (h_iqn, t_iqn))
                #print ses_scsi_ids
                #if lun_scsi_ids:
                    #for l_id in lun_scsi_ids:
                        #_print("DEBUG: checking if %s is on ses_ids" % l_id)
                        #print ses_scsi_ids
                        #if l_id in ses_scsi_ids:
                            #if not found_scsi_ids:
                                #found_scsi_ids = []
                            #found_scsi_ids.append(l_id)
                #else:
                    #_print("FATAL: scsi_id_of_lun - TODO")

        #if not found_scsi_ids:
            #_print("FAIL: scsi_id_of_lun(): No SCSI disk found on host for %s" % lun_name)
            #return None

        return lun_scsi_ids

    def lun_query(self):
        """
        Get all a list with all LUN names
        """
        capability = self.capability()
        if not capability or "lun_query" not in capability or not capability["lun_query"]:
            _print("FAIL: lun_query(). SanMgmt can not perform command on:")
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            if capability:
                print capability
            return None

        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FATAL: lun_query(): Failed to sa_obj even though it pass capability check")
            return None
        return sa_obj.query_all_luns()

    def lun_info(self, lun_name):
        """
        Query details of specific LUN
        """
        if not lun_name:
            _print("FAIL: lun_info() - requires lun_name parameter")
            return None

        if not self.is_config_loaded():
            _print("FAIL: lun_info() - Please call obj->load_conf() first")
            return None

        capability = self.capability()
        if not capability or "lun_info" not in capability or not capability["lun_info"]:
            _print("FAIL: lun_info(). SanMgmt can not perform command on")
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            if capability:
                print capability
            return None

        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: lun_info(): Failed to sa_obj even though it pass capability check")
            return None
        return sa_obj.lun_info(lun_name)

    def lun_create(self, size=None, lun_name=None, thinp=None):
        """
        Usage
            obj.lun_create($size)
            obj.lun_create(size=>size)
            obj.lun_create(size=>size,lun_name=>lun_name)
            obj.lun_create(size=>size,lun_name=>lun_name, thinp=>flag_thinp)
        Purpose
            Create LUN with size named as lun_name. Please be informed this
            method DO NOT map LUN to any host, please call obj->lun_map() or
            obj->lun_create_and_map().
            If lun_name is undefined, we will let Storage Array control module
            to generate base on their own configuration:
            If flag_thinp is defined, will try not reserse all space it requested
            by using Thin-Provisioning technology. It need storage array report
            capability of "lun_thinp"
        Parameters
            size       # size, accept example: "1B", "1KiB", "2.1GiB", "3GiB", "2TiB"
            lun_name   # the LUN name on storage array. like:
                        #   /vol/vol_storageqe/storageqe_06-19
        Returns
            lun_name
                or
            None
        """
        if not size:
            _print("FAIL: lun_create requires size parameter")
            return None
        if not self.is_config_loaded():
            _print("FAIL: lun_create() - Please call obj->load_conf() first")
            return None

        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: lun_create(): Failed to sa_obj even though it pass capability check")
            return None

        capability = sa_obj.capability()
        if not capability or "lun_create" not in capability or not capability["lun_create"]:
            _print("FAIL: lun_create(). SanMgmt can not perform command on")
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            if capability:
                print capability
            return None

        sa_val = self.get_sa_self()
        if not sa_val:
            _print("FAIL: lun_create(): Failed to sa_self even though it pass capability check")
            return None

        sa_model = sa_val["model"]
        if thinp:
            if "lun_thinp" not in capability or not capabiltity["lun_thinp"]:
                _print("FAIL: lun_create() - Thin Provisioning of LUN not supported on %s" % model)
                return None

        if not size_human_check(size):
            _print("FAIL: lun_create() - Incorrect format for size '%s'.")
            print ("acceptable formats are: 5B, 1KiB, 2.1MiB, 3GiB, 4TiB")
            return None

        size_bytes = size_human_2_size_bytes(size)
        if not size_bytes:
            _print("FAIL: lun_create() - LUN size should be > 1 byte")
            return None

        _print("DEBUG: Create LUN with size %s bytes" % size_bytes)

        #_print("DEBUG: Storage array version: %s" % sa_obj.get_version())
        if lun_name:
            if "lun_path" in sa_val:
                lun_name = "%s%s" % (sa_val["lun_path"], lun_name)
        else:
            lun_name = "tmp"
            #Check if prefix option is set on config file
            if "auto_lun_create_prefix" in sa_val:
                lun_name = sa_val["auto_lun_create_prefix"]
            lun_name += linux.time_stamp()

        return sa_obj.lun_create(lun_name, size_bytes)

    def lun_remove(self, lun_name):
        """
        Usage
            obj->lun_remove(lun_name)
        Purpose
            Delete LUN lun_name from storage array
        Parameters
            lun_name        # the LUN name on storage array.
        Returns
            True
                or
            False
        """
        if not lun_name:
            _print("FAIL: lun_remove - requires lun_name parameter")
            return None

        if not self.is_config_loaded():
            _print("FAIL: lun_map() - Please call obj->load_conf() first")
            return None


        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: lun_remove(): Failed to sa_obj even though it pass capability check")
            return None

        capability = sa_obj.capability()
        if not capability or "lun_remove" not in capability or not capability["lun_remove"]:
            _print("FAIL: lun_remove(). SanMgmt can not perform command on")
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            if capability:
                print capability
            return None

        sa_val = self.get_sa_self()
        if not sa_val:
            _print("FAIL: lun_remove(): Failed to sa_self even though it pass capability check")
            return None

        #If we are removing lun for Equallogic array, we need to delete the target
        #as each LUN is one target...
        model = sa_val["model"]
        if model == "DELL-EQLOGIC":
            info_dict = self.lun_info(lun_name)
            t_iqns = iscsi.t_iqn_of_sessions()
            if info_dict and t_iqns:
                if info_dict["iscsi_name"] in t_iqns:
                    iscsi.node_logout("-T %s" % info_dict["iscsi_name"])
                if iscsi.is_target_discovered(info_dict["iscsi_name"]):
                    iscsi.node_delete("-T %s" % info_dict["iscsi_name"])

        #sometimes the removal can fail, specially if there is IO being execute there
        #try it few times
        attempt = 5
        while attempt > 0:
            if sa_obj.lun_remove(lun_name):
                return True
            linux.sleep(1)
            attempt -= 1
        _print("FAIL: Could not delete lun %s" % lun_name)
        return False

    def lun_map(self, lun_name, t_addr=None, i_addr=None, lun_id=None, rescan=False):
        """
        Usage
            obj->lun_map(lun_name, init_names)
            obj->lun_map(lun_name, init_names, rescan=>$flag_rescan)
        Purpose
            Map LUN lun_name to host initiator
        Parameters
            lun_name        # the LUN name on storage array.
            lun_id          # the LUN ID host should get
            $flag_rescan
        Returns
            True
                or
            False
        """
        if not lun_name:
            _print("FAIL: lun_map - requires lun_name and init_names parameters")
            return False

        if not self.is_config_loaded():
            _print("FAIL: lun_map() - Please call obj->load_conf() first")
            return False

        capability = self.capability()
        if not capability or "lun_map" not in capability or not capability["lun_map"]:
            _print("FAIL: lun_map(). SanMgmt can not perform command on")
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            if capability:
                print capability
            return False

        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: lun_map(): Failed to sa_obj even though it pass capability check")
            return None

        map_info = self.map_info()
        #There is no map info, probably because self was not created based on existing mpath device
        if not map_info or t_addr or i_addr:
            #If there is no mapping given we will try to map
            #every host to all targets, unless some target or initiator address is given
            map_info = []
            t_wwpns = self.t_wwpns()
            h_wwpns = self.h_wwpns()
            t_iqns = self.t_iqns()
            h_iqns = self.h_iqns()
            if t_addr:
                if fc.is_wwn(t_addr):
                    t_wwpns = [t_addr]
                elif iscsi.is_iqn(t_addr):
                    t_iqns = [t_addr]
                else:
                    _print("FAIL: lun_map() - Unsupported t_addr %s" % t_addr)
                    return False

            if i_addr:
                if fc.is_wwn(i_addr):
                    h_wwpns = [i_addr]
                elif iscsi.is_iqn(i_addr):
                    h_iqns = [i_addr]
                else:
                    _print("FAIL: lun_map() - Unsupported i_addr %s" % i_addr)
                    return False

            for t_wwpn in t_wwpns:
                for h_wwpn in h_wwpns:
                    map_info.append({"t_wwpn" : t_wwpn, "h_wwpn" : h_wwpn})
            for t_iqn in t_iqns:
                for h_iqn in h_iqns:
                    map_info.append({"t_iqn" : t_iqn, "h_iqn" : h_iqn})

        sa_obj.map_info = map_info
        if not sa_obj.lun_map(lun_name):
            return False

        #We only do this for iSCSI
        obj_t_iqns = self.t_iqns()
        obj_h_iqns = self.h_iqns()
        if obj_t_iqns and obj_h_iqns:
            #Equallogic creates new targets per each LUN
            #We need to discover this new target...
            info_dict = self.lun_info(lun_name)
            if not info_dict:
                _print("FAIL: lun_map() - Could not get info of lun %s" % lun_name)
                return False

            if "iscsi_name" in info_dict.keys():
                new_t_iqn = info_dict["iscsi_name"]
                if not iscsi.is_target_discovered(new_t_iqn):
                    #We discover target using the same interface used on original target
                    disc_ifaces = iscsi.get_disc_ifaces_of_t_iqn(obj_t_iqns[0])
                    if not disc_ifaces:
                        _print("FAIL: lun_map() - Can not map %s because we could not find the interfaces used to connect to original target %s"
                               % (new_t_iqn, obj_t_iqns[0]))
                        return False
                    ifaces = " ".join(disc_ifaces)
                    sa_val = self.get_sa_self()
                    if not sa_val:
                        _print("FAIL: lun_map(): Failed to sa_self even though it pass capability check")
                        return None
                    if not iscsi.discovery_st(sa_val["ctrl_ip"], ifaces):
                        _print("FAIL: Could not discover iSCSI target after mapping new LUN")
                        return False
                if info_dict["iscsi_name"] not in iscsi.t_iqn_of_sessions():
                    if not iscsi.node_login("-T %s" % info_dict["iscsi_name"]):
                        _print("FAIL: Could not discover iSCSI target after mapping new LUN")
                        return False

        if rescan:
            hosts = scsi.get_hosts()
            for host in hosts:
                scsi.rescan_host(host)
            linux.wait_udev()

        return True

    def lun_unmap(self, lun_name, t_addr=None, i_addr=None, lun_id=None):
        """
        Usage
            obj->lun_unmap(lun_name, init_names)
            obj->lun_unmap(lun_name, init_names)
        Purpose
            Map LUN lun_name to host initiator
        Parameters
            lun_name        # the LUN name on storage array.
            lun_id          # the LUN ID host should get
            $flag_rescan
        Returns
            True
                or
            False
        """
        if not lun_name:
            _print("FAIL: lun_unmap - requires lun_name and init_names parameters")
            return False

        if not self.is_config_loaded():
            _print("FAIL: lun_unmap() - Please call obj->load_conf() first")
            return False

        capability = self.capability()
        if not capability or "lun_unmap" not in capability or not capability["lun_unmap"]:
            _print("FAIL: lun_unmap(). SanMgmt can not perform command on")
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            if capability:
                print capability
            return False

        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: lun_unmap(): Failed to sa_obj even though it pass capability check")
            return None

        map_info = self.map_info()
        #There is no map info, probably because self was not created based on existing mpath device
        if not map_info or t_addr or i_addr:
            #If there is no mapping given we will try to map
            #every host to all targets, unless some target or initiator address is given
            map_info = []
            t_wwpns = self.t_wwpns()
            h_wwpns = self.h_wwpns()
            t_iqns = self.t_iqns()
            h_iqns = self.h_iqns()
            if t_addr:
                if fc.is_wwn(t_addr):
                    t_wwpns = [t_addr]
                elif iscsi.is_iqn(t_addr):
                    t_iqns = [t_addr]
                else:
                    _print("FAIL: lun_unmap() - Unsupported t_addr %s" % t_addr)
                    return False

            if i_addr:
                if fc.is_wwn(i_addr):
                    h_wwpns = [i_addr]
                elif iscsi.is_iqn(i_addr):
                    h_iqns = [i_addr]
                else:
                    _print("FAIL: lun_unmap() - Unsupported i_addr %s" % i_addr)
                    return False

            for t_wwpn in t_wwpns:
                for h_wwpn in h_wwpns:
                    map_info.append({"t_wwpn" : t_wwpn, "h_wwpn" : h_wwpn})
            for t_iqn in t_iqns:
                for h_iqn in h_iqns:
                    map_info.append({"t_iqn" : t_iqn, "h_iqn" : h_iqn})

        sa_obj.map_info = map_info
        if not sa_obj.lun_unmap(lun_name):
            return False

        #If we are removing lun for Equallogic array, we need to delete the target
        #as each LUN is one target...
        sa_val = self.get_sa_self()
        if not sa_val:
            _print("FAIL: lun_unmap(): Failed to sa_self even though it pass capability check")
            return None
        model = sa_val["model"]
        if model == "DELL-EQLOGIC":
            info_dict = self.lun_info(lun_name)
            if info_dict:
                if info_dict["iscsi_name"] in iscsi.t_iqn_of_sessions():
                    iscsi.node_logout("-T %s" % info_dict["iscsi_name"])
                if iscsi.is_target_discovered(info_dict["iscsi_name"]):
                    iscsi.node_delete("-T %s" % info_dict["iscsi_name"])

        return True


    def lun_create_and_map(self, size=None, lun_name=None, rescan=None):
        """
        Usage
            obj.lun_create_and_map(size, init_names)
            obj.lun_create_and_map(size=size,lun_name=lun_name, init_name=ini_names)
            obj.lun_create_and_map(size=size,init_names=init_names)
            obj.lun_create_and_map(size=size,rescan=flag_rescan)
        Purpose
            Just automatically call lun_create() and lun_map(). If you want to
            rescan after creation, enable flag_rescan.
        Parameters
            size
            lun_name       # optional.
            lun_id         # optional.
            flag_rescan
        Returns
            lun_name
                or
            undef
        """

        lun_name = self.lun_create(size = size, lun_name=lun_name)
        if not lun_name:
            _print("FAIL: lun_create_and_map(): Skip mapping since LUN creation failed")
            return None
        if not self.lun_map(lun_name, rescan=rescan):
            _print("FAIL: lun_create_and_map - Could not map %s" % (lun_name))
            _print("INFO: Deleting just created LUN")
            self.lun_remove(lun_name)
            return None
        return lun_name

    def get_controller_names(self):
        """
        """
        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: get_controller_names(): Failed to sa_obj even though it pass capability check")
            return None

        sa_conf_dict = sa_obj.get_sa_conf()

        if not sa_conf_dict:
            _print("FAIL: get_controller_names() - Controller is not configured on san_top config")
            return None

        ctrler_names = []
        ctrler_name_regex = re.compile("ctrl_ip_(.*)")
        for key in sa_conf_dict.keys():
            m = ctrler_name_regex.match(key)
            if m:
                ctrler_names.append(m.group(1))
        return ctrler_names

    def sa_t_wwpn_2_ctrler(self):
        """

        """
        ctrlers = self.get_controller_names()
        if not ctrlers:
            return None


        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: sa_t_wwpn_2_ctrler(): Failed to sa_obj for Storage Array")
            return None

        sa_conf_dict = sa_obj.get_sa_conf()

        sa_t_wwpn_2_ctrler = {}
        for ctrler in ctrlers:
            cfg_ctrl_name = "ctrl_ip_%s" % ctrler
            ctrler_ip = sa_conf_dict[cfg_ctrl_name]
            sa_t_wwpn_2_ctrler[ctrler] = sa_obj.sa_ctrler_t_wwpns(ctrler_ip)

        return sa_t_wwpn_2_ctrler


    def get_controller_ip_of_port(self, t_addr):
        """
        From a given port gets the controller IP address that uses it
        This information is from san_top config file
        """
        if not t_addr:
            _print("FAIL: get_controller_ip_of_port() - requires controller_port as parameter")
            return None

        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: get_controller_ip_of_port(): Failed to sa_obj even though it pass capability check")
            return None

        sa_conf_dict = sa_obj.get_sa_conf()

        if "port_of" in sa_conf_dict and t_addr in sa_conf_dict["port_of"].keys():
            ctrler_port = sa_obj.sa_conf_dict["port_of"][t_addr]
            #spa-fc1 becomes spa
            ctrler_name = re.sub("-.*","", ctrler_port)
            cfg_ctrl_name = "ctrl_ip_%s" % ctrler_name
            if cfg_ctrl_name not in sa_conf_dict:
                _print("FAIL: sa_ctrler_check() - %s is not defined on config file" % cfg_ctrl_name)
                for san_cfg in self.san_conf_list:
                    if san_cfg["name"] == "sa_conf":
                        print san_cfg
                return None
            ctrler_ip = sa_conf_dict[cfg_ctrl_name]
            return ctrler_ip
        return None

    def sa_ctrler_reboot(self, t_addr):
        """
        Usage
            obj->sa_ctrler_reboot(t_addr)
        Purpose
            Reboot controller with given wwwpn/iqn
        Parameters
            t_addr          # Controller WWPN or IQN
        Returns
            True
                or
            False
        """
        if not t_addr:
            _print("FAIL: sa_ctrler_reboot() - requires t_addr parameters")
            return False

        if not self.is_config_loaded():
            _print("FAIL: sa_ctrler_reboot() - Please call obj->load_conf() first")
            return False

        capability = self.capability()
        if not capability or "sa_ctrler_reboot" not in capability or not capability["sa_ctrler_reboot"]:
            _print("FAIL: sa_ctrler_reboot(). SanMgmt can not perform command on")
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            if capability:
                print capability
            return False

        ctrler_ip = self.get_controller_ip_of_port(t_addr)
        if not ctrler_ip:
            _print("FAIL: sa_ctrler_reboot() - Could not find controller IP for port %s" % t_addr)
            return False

        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: sa_ctrler_reboot(): Failed to sa_obj even though it pass capability check")
            return None

        if not sa_obj.sa_ctrler_reboot(ctrler_ip):
            return False
        return True


    def sa_ctrler_check(self, t_addr):
        """
        Usage
            obj->sa_ctrler_check(t_addr)
        Purpose
            Check controller with given wwwpn/iqn
        Parameters
            t_addr          # Controller WWPN or IQN
        Returns
            True
                or
            False
        """
        if not t_addr:
            _print("FAIL: sa_ctrler_check() - requires t_addr parameters")
            return None

        if not self.is_config_loaded():
            _print("FAIL: sa_ctrler_check() - Please call obj->load_conf() first")
            return None

        capability = self.capability()
        if not capability or "sa_ctrler_reboot" not in capability or not capability["sa_ctrler_reboot"]:
            _print("FAIL: sa_ctrler_check(). SanMgmt can not perform sa_ctrler_reboot, so do not allow ctrler_check")
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            if capability:
                print capability
            return None

        sa_obj = self.get_sa_obj_self()
        if not sa_obj:
            _print("FAIL: sa_ctrler_check(): Failed to sa_obj even though it pass capability check")
            return None

        ctrler_ip = self.get_controller_ip_of_port(t_addr)
        if not ctrler_ip:
            _print("FAIL: sa_ctrler_check() - Could not find controller IP for port %s" % t_addr)
            return None

        return sa_obj.sa_ctrler_check(ctrler_ip)

    def sa_ctrler_wait(self, t_addr, timeout=1800, interval=30):
        """
        Usage
            obj->sa_ctrler_wait(t_addr)
        Purpose
            Wait for specific controller to be online
        Parameters
            t_addr          # Controller WWPN or IQN
        Returns
            True
                or
            False
        """
        if not t_addr:
            _print("FAIL: sa_ctrler_wait() - requires t_addr parameters")
            return False

        if not self.is_config_loaded():
            _print("FAIL: sa_ctrler_wait() - Please call obj->load_conf() first")
            return False

        capability = self.capability()
        if not capability or "sa_ctrler_reboot" not in capability or not capability["sa_ctrler_reboot"]:
            _print("FAIL: sa_ctrler_wait(). SanMgmt can not perform sa_ctrler_reboot, so do not allow sa_ctrler_wait")
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            if capability:
                print capability
            return False


        ctrler_ip = self.get_controller_ip_of_port(t_addr)
        if not ctrler_ip:
            _print("FAIL: sa_ctrler_wait() - Could not find controller IP for port %s" % t_addr)
            for san_cfg in self.san_conf_list:
                if san_cfg["name"] == "sa_conf":
                    print san_cfg
            return False


        loop_count = int(timeout/interval)
        while loop_count > 1:
            loop_count -= 1
            status = self.sa_ctrler_check(t_addr)
            if not status:
                _print("FAIL: sa_ctrler_wait() - Could not get status of controller %s" % ctrler_ip)
                return False
            if status == "online":
                return True
            _print("INFO: Controller still on status: %s (loop_count: %s)" % (status, loop_count))
            linux.sleep(interval)

        _print("FAIL: sa_ctrler_wait(): Timeout, controller %s still offline" % ctrler_ip)
        return False

    def iscsi_host_conf(self):
        """
        Based on hostname information get host iSCSI configuration
        """
        if not self.is_config_loaded():
            self.load_conf()

        if not self.is_config_loaded():
            _print("FAIL: iscsi_host_conf() - Config file was not loaded")
            return None

        host = linux.hostname()

        iscsi_conf_dict = None
        for config_entry in self.san_conf_list:
            if config_entry["name"] == "iscsi_conf":
                iscsi_conf_dict = config_entry["hosts"]

        if not iscsi_conf_dict:
            #_print("DEBUG: iscsi_host_conf() - no iSCSI host settings on san_conf_file")
            #print self.san_conf_list.keys()
            return None

        if host not in iscsi_conf_dict.keys():
            #_print("DEBUG: iscsi_host_conf() - there is no iSCSI settings for "
            #    + "%s on san_conf_file" % host)
            #print iscsi_conf_dict
            return None

        iscsi.install()

        host_iscsi_ifaces = iscsi.get_iscsi_iface_names()
        if not host_iscsi_ifaces:
            _print("FAIL: iscsi_host_conf() - Host does not have iSCSI interface")
            return None

        host_conf = iscsi_conf_dict[host]
        iface_name_ipv4_suffix = ".ipv4.0"
        iface_name_ipv6_suffix = ".ipv6.0"
        for iface in host_conf["ifaces"].keys():
            found_iface = False
            #iscsiadm might append IP version to interface name, we update server config to reflect it
            if "name" in host_conf["ifaces"][iface].keys():
                iface_name = host_conf["ifaces"][iface]["name"]
                #Check if host has this interface
                for host_iface in host_iscsi_ifaces:
                    if iface_name == host_iface:
                        found_iface = True
                        break
                    ipv4_iface = "%s%s" % (iface_name, iface_name_ipv4_suffix)
                    if ipv4_iface == host_iface:
                        found_iface = True
                        host_conf["ifaces"][iface]["name"] = host_iface
                        break

                if not found_iface:
                    _print("FAIL: iscsi_host_conf() - %s is configured on san_top, but does not exist on host" % iface_name)
                    return None

        return host_conf

    def setup_iscsi(self):
        """
        Usage
            obj.setup_iscsi()
        Purpose
            Setup iSCSI Host base on san_conf
        Parameter
            san_conf     # conf file path
        Returns
            scsi_ids   # the iSCSI disks found
                or
            None
        """
        host_conf = self.iscsi_host_conf()
        if not host_conf:
            return False

        if "ifaces" not in host_conf.keys():
            _print("FAIL: setup_iscsi() - there is no iSCSI iface defined on config file for host")
            print host_conf
            return False

        success = True

        for iface in host_conf["ifaces"].keys():
            iface_name = "default"
            portal = None
            iqn = None
            iface_ip = None

            if "name" in host_conf["ifaces"][iface].keys():
                iface_name = host_conf["ifaces"][iface]["name"]
            if "target_ip" in host_conf["ifaces"][iface].keys():
                portal = host_conf["ifaces"][iface]["target_ip"]
            if "iqn" in host_conf["ifaces"][iface].keys():
                iqn = host_conf["ifaces"][iface]["iqn"]
            if "ip" in host_conf["ifaces"][iface].keys():
                iface_ip = host_conf["ifaces"][iface]["ip"]

            if not iface_name or not portal or not iqn:
                _print("FAIL: setup_iscsi() - interface %s requires iface_name, portal and iqn info to be set on config file" % iface)
                print host_conf["ifaces"][iface]
                success = False
                continue

            if iface_ip:
                if not iscsi.iface_set_ip(iface_name, iface_ip):
                    _print("FAIL: setup_iscsi() - Could not set IP for %s" % iface_name)
                    success = False
                    continue
            if not iscsi.iface_set_iqn(iface_name, iqn):
                _print("FAIL: setup_iscsi() - Could not set %s to iface %s" % (iqn, iface_name))
                success = False
                continue
            if not iscsi.discovery_st(portal, ifaces=iface_name, disc_db=True):
                _print("FAIL: setup_iscsi() - Could not discover any target on %s using iface %s" % (portal, iface_name))
                success = False
                continue
            if not iscsi.node_login():
                _print("FAIL: setup_iscsi() - Could not login to new discovered portals")
                success = False
                continue
            print("INFO: %s logged in successtully to %s" % (iface_name, portal))

        if success:
            return True
        return False

    def _addr_of(self, addr):
        """
        """
        if fc.standardize_wwpn(addr):
            return fc.standardize_wwpn(addr)
        if net.standardize_mac(addr):
            return net.standardize_mac(addr)
        _print("FAIL: %s is neither a valid WWPN/MAC address.")
        return None


    def rcsn_trigger(self, action=None, wwpn=None, addr=None):
        """
        Usage
            obj.rcsn_trigger(action=action, addr=wwpn)
            obj.rcsn_trigger(action=action, addr=mac)
        Purpose
            Enable/Disable RCSN messages to be sent on this port
        Parameters
            action     # "UP" or "DOWN"
            addr       # MAC address or WWPN address
        Returns
            True
                or
            False
        """
        if wwpn:
            addr = wwpn

        if not addr or not action:
            _print("FAIL: link_trigger() - requires action and addr parameters")
            return False

        if not self.is_config_loaded():
            _print("FAIL: link_trigger() - Please call obj->load_conf() first")
            return False

        #make sure action is always uppercase
        action = action.upper()

        need_capability = None
        if action == "ENABLE":
            need_capability = "rcsn_enable"
        if action == "DISABLE":
            need_capability = "rcsn_disable"
        if not need_capability:
            _print("FAIL: rcsn_trigger() - Unsupported action: %s" % action)
            return False

        #Get the switch connected to this port
        connected_sw = self.get_sw_self(addr)
        if not connected_sw:
            _print("FAIL: rcsn_trigger(): No switch in configuration is "
                + "controlling %s, even though it pass config check..." % addr)
            return False

        sw_obj = connected_sw["class_obj"]

        if need_capability not in sw_obj.capability():
            _print("FAIL: rcsn_trigger() - Switch does not support %s" % need_capability)
            print connected_sw
            return False

        if "port_of" in connected_sw.keys() and addr in connected_sw["port_of"]:
            return sw_obj.rcsn_trigger(action=action, port_id=connected_sw["port_of"][addr])

        _print("FAIL: rcsn_trigger() - Could not find which interface port %s is connected to" % addr)
        return False

    def link_trigger(self, action=None, wwpn=None, addr=None):
        """
        Usage
            obj.link_trigger(action=action, addr=wwpn)
            obj.link_trigger(action=action, addr=mac)
        Purpose
            Bring switch port UP or DOWN which connected to $wwpn.
        Parameters
            action     # "UP" or "DOWN"
            addr       # MAC address or WWPN address
        Returns
            True
                or
            False
        """
        if wwpn:
            addr = wwpn

        if not addr or not action:
            _print("FAIL: link_trigger() - requires action and addr parameters")
            return False

        if not self.is_config_loaded():
            _print("FAIL: link_trigger() - Please call obj->load_conf() first")
            return False

        #make sure action is always uppercase
        action = action.upper()

        capability = self.capability()
        need_capability = None
        if action == "UP":
            need_capability = "link_up"
        if action == "DOWN":
            need_capability = "link_down"
        if not need_capability:
            _print("FAIL: link_trigger() - Unsupported action: %s" % action)
            return False

        #flag_capability_pass = False
        #if need_capability in capability.keys():
        #    if addr in capability[need_capability]:
        #        flag_capability_pass = True
        #if not flag_capability_pass:
        #    _print("FAIL: link_trigger() - Is not possible to execute %s on port %s" % (need_capability, addr))
        #    print capability
        #    return False

        #Get the switch connected to this port
        connected_sw = self.get_sw_self(addr)
        if not connected_sw:
            _print("FAIL: link_trigger(): No switch in configuration is "
                + "controlling %s, even though it pass config check..." % addr)
            return False

        sw_obj = connected_sw["class_obj"]

        if need_capability not in sw_obj.capability():
            _print("FAIL: link_trigger() - Switch does not support %s" % need_capability)
            print connected_sw
            return False

        if "port_of" in connected_sw.keys() and addr in connected_sw["port_of"]:
            return sw_obj.link_trigger(action=action, port_id=connected_sw["port_of"][addr])

        _print("FAIL: link_trigger() - Could not find which interface port %s is connected to" % addr)
        return False

    def link_up(self, addr):
        return self.link_trigger(action="UP", addr=addr)

    def link_down(self, addr):
        return self.link_trigger(action="DOWN", addr=addr)

    def port_state(self, addr):
        """
        Check switch port state on of a WWPN/MAC
        """
        #Get the switch connected to this port
        connected_sw = self.get_sw_self(addr)
        if not connected_sw:
            _print("FAIL: port_state(): No switch in configuration is "
                + "controlling %s, even though it pass capability check..." % addr)
            return None

        sw_obj = connected_sw["class_obj"]
        if "port_of" in connected_sw.keys() and addr in connected_sw["port_of"]:
            return sw_obj.port_state(connected_sw["port_of"][addr])

        _print("FAIL: port_state() - Could not find which interface port %s is connected to" % addr)
        return None

    def phy_link_trigger(self, action=None, addr=None):
        """
        Usage
            obj.phy_link_trigger(action=>$action, addr=>wwpn)
            obj.phy_link_trigger(action=>$action, addr=>mac)
        Purpose
            Bring physical switch port UP or DOWN which connected to addr.
        Parameters
            action     # "UP" or "DOWN"
            addr       # Port ID
        Returns
            True
                or
            False
        """
        if not addr or not action:
            _print("FAIL: phy_link_trigger() - requires action and addr parameters")
            return False

        if not self.is_config_loaded():
            _print("FAIL: phy_link_trigger() - Please call obj->load_conf() first")
            return False

        #make sure action is always uppercase
        action = action.upper()
        capability = self.capability()
        need_capability = None
        if action == "UP":
            need_capability = "phy_port_connect"
        if action == "DOWN":
            need_capability = "phy_port_disconnect"
        if not need_capability:
            _print("FAIL: phy_link_trigger() - Unsupported action: %s" % action)
            return False

        flag_capability_pass = False
        if need_capability in capability.keys():
            if addr in capability[need_capability]:
                flag_capability_pass = True
        if not flag_capability_pass:
            _print("FAIL: phy_link_trigger() - Is not possible to execute %s on port %s" % (need_capability, addr))
            print capability
            return False

        #Get the switch connected to this port
        connected_sw = self.get_physw_self(addr)
        if not connected_sw:
            _print("FAIL: phy_link_trigger(): No switch in configuration is "
                + "controlling %s, even though it pass capability check..." % addr)
            return False


        sw_obj = connected_sw["class_obj"]
        if "port_of" in connected_sw.keys() and addr in connected_sw["port_of"]:
            if action == "DOWN":
                return sw_obj.port_disconnect(connected_sw["port_of"][addr])
            if action == "UP":
                port_id = connected_sw["port_of"][addr]
                if "simplex_con" in connected_sw.keys():
                    if port_id in connected_sw["simplex_con"].keys():
                        dst_port = connected_sw["simplex_con"][port_id]
                        return sw_obj.port_connect(port_id, dst_port, "simplex")
                if "duplex_con" in connected_sw.keys():
                    if port_id in connected_sw["duplex_con"].keys():
                        dst_port = connected_sw["duplex_con"][port_id]
                        return sw_obj.port_connect(port_id, dst_port, "duplex")


        _print("FAIL: phy_link_trigger() - Could not find which interface port %s is connected to" % addr)
        return False

    def phy_link_flap(self, addr, uptime, downtime, count):
        """
        Usage
            obj.phy_link_flap(addr, uptime, downtime, count)
        Purpose
            Flap a port. This method will wait until flap done.
            Some physical switch model (e.g. ApCon) has maximum time limited, they will
            be wraped down to maximum value.
        Parameters
            addr           # WWPN or MAC address
            uptime         # time to keep link up during flap in micro seconds
            downtime       # time to keep link down during flap in micro seconds
            count          # how many times should we flap the port. like '10'
        Returns
            True
                or
            False
        """
        if not addr or not uptime or not downtime or not count:
            _print("FAIL: phy_link_flap() - requires addr, uptime, downtime and count parameters")
            return False

        if not self.is_config_loaded():
            _print("FAIL: phy_link_flap() - Please call obj->load_conf() first")
            return False

        capability = self.capability()
        need_capability = "phy_port_flap"
        flag_capability_pass = False
        if need_capability in capability.keys():
            if addr in capability[need_capability]:
                flag_capability_pass = True
        if not flag_capability_pass:
            _print("FAIL: phy_link_flap() - Is not possible to execute %s on port %s" % (need_capability, addr))
            print capability
            return False

        #Get the switch connected to this port
        connected_sw = self.get_physw_self(addr)
        if not connected_sw:
            _print("FAIL: phy_link_flap(): No switch in configuration is "
                + "controlling %s, even though it pass capability check..." % addr)
            return False


        sw_obj = connected_sw["class_obj"]
        if "port_of" in connected_sw.keys() and addr in connected_sw["port_of"]:
            return sw_obj.port_flap(connected_sw["port_of"][addr], uptime, downtime, count)

        _print("FAIL: phy_link_flap() - Could not find which interface port %s is connected to" % addr)
        return False
