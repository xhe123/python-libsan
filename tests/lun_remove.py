#!/usr/bin/python
# Copyright (C) 2016 Red Hat, Inc.
# This file is part of libsan.
#
# libsan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libsan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libsan.  If not, see <http://www.gnu.org/licenses/>.

import libsan.sanmgmt as sanmgmt
import libsan.host.linux as linux
import sys

if len(sys.argv) < 2:
    print("FAIL: required lun_name as parameter")
    sys.exit(1)

lun_name = sys.argv[1]

#Create SanMgmt object based on all HW settings on the server
#This might be problematic it host is connected to more then 1 Storage Array
obj_sanmgmt = sanmgmt.create_sanmgmt_for_mpath()
if not obj_sanmgmt:
    print("FAIL: Could not create SanMgmt object")
    sys.exit(1)

#Check LUN wwid
wwid = obj_sanmgmt.wwid_of_lun(lun_name)
if not wwid:
    print("FAIL: Could not find WWID for %s", lun_name)
    sys.exit(1)

if not linux.remove_device_wwid(wwid):
    print("FAIL: Could not remove LUN %s from the host" % lun_name)
    sys.exit(1)

if not obj_sanmgmt.lun_remove(lun_name):
    print("FAIL: Could not remove LUN from storage array")
    sys.exit(1)

print("PASS: LUN %s successfully removed" % lun_name)
sys.exit(0)
