#!/usr/bin/python
# Copyright (C) 2016 Red Hat, Inc.
# This file is part of libsan.
#
# libsan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libsan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libsan.  If not, see <http://www.gnu.org/licenses/>.

import libsan.sanmgmt as sanmgmt
import libsan.host.linux as linux
import libsan.host.mp as mp
import sys

if len(sys.argv) < 2:
    print("FAIL: requires mpath_name")
    sys.exit(1)


mpath_name = sys.argv[1]
obj_sanmgmt = sanmgmt.create_sanmgmt_for_mpath(mpath_name)
if not obj_sanmgmt:
    print("FAIL: Could not create SAN management class for %s" % mpaths[0])
    sys.exit(1)

host_ports = []
h_wwpns = obj_sanmgmt.h_wwpns()
if h_wwpns:
    host_ports.extend(h_wwpns)

iscsi_macs = obj_sanmgmt.macs()
if iscsi_macs:
    host_ports.extend(iscsi_macs)

if not host_ports:
    print("FAIL: Could not find any host port to use")
    sys.exit(1)

print("INFO: Host has the following host ports:")
print host_ports

error = 0
scsi_devices = mp.get_disks_of_mpath(mpath_name)
if not scsi_devices:
    print("FAIL: Could not find any SCSI device on mpath %s" % mpath_name)
    sys.exit(1)

for port in host_ports:
    print("INFO: Bringing port %s DOWN" % port)
    if not obj_sanmgmt.link_trigger(action="DOWN", addr=port):
        print("FAIL: Could not bring port %s DOWN" % port)
        error += 1
    print("INFO: Waiting server to detect link change")
    linux.sleep(120)
    for scsi_device in scsi_devices:
        status = mp.mpath_check_disk_dm_status(mpath_name, scsi_device)
        print "INFO: SCSI device %s has status %s" % (scsi_device, status)

    print("INFO: Bringing port %s UP" % port)
    if not obj_sanmgmt.link_trigger(action="UP", addr=port):
        print("FAIL: Could not bring port %s UP" % port)
        error += 1
    print("INFO: Waiting server to detect link change")
    linux.sleep(120)
    for scsi_device in scsi_devices:
        status = mp.mpath_check_disk_dm_status(mpath_name, scsi_device)
        print "INFO: SCSI device %s has status %s" % (scsi_device, status)

if error:
    print("FAIL: There was some error bring switch port DOWN and UP")
    sys.exit(1)

print("PASS: Switch ports have been brought DOWN and UP")
sys.exit(0)

