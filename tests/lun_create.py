#!/usr/bin/python
# Copyright (C) 2016 Red Hat, Inc.
# This file is part of libsan.
#
# libsan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# libsan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libsan.  If not, see <http://www.gnu.org/licenses/>.

import libsan.sanmgmt as sanmgmt
import libsan.host.fcoe as fcoe
import libsan.host.scsi as scsi
import libsan.host.mp as mp
import sys

#Try to enable soft FCoE
fcoe.setup_soft_fcoe()

#Try to enable iSCSI
sanmgmt.setup_iscsi()

#Create an object using the information from an existing multipath device
#We do not ming if device is used for LVM or boot as we only use it to get LUN map info
choosed_mpath_dict = sanmgmt.choose_mpaths(exclude_boot_device=False, exclude_lvm_device=False)
if not choosed_mpath_dict:
    print("FAIL: Could not find any multipath device on host")
    sys.exit(1)

mpath_names = choosed_mpath_dict.keys()
obj_sanmgmt = sanmgmt.create_sanmgmt_for_mpath(mpath_names[0])
if not obj_sanmgmt:
    print("FAIL: Could not create SAN management class for %s" % mpaths[0])
    sys.exit(1)

tmp_lun = obj_sanmgmt.lun_create_and_map(size="1GiB", rescan=True)
if not tmp_lun:
    print("FAIL Could not create or map tmp LUN")
    sys.exit(1)

scsi_ids = obj_sanmgmt.scsi_id_of_lun(lun_name=tmp_lun)
if not scsi_ids:
    print("FAIL: Failed to find out the SCSI ID for newly mapped LUN %s" % tmp_lun)
    obj_sanmgmt.lun_remove(tmp_lun)
    sys.exit(1)

print("INFO: New SCSI disks created:")
for _ids in scsi_ids:
    print("    %s - %s" % (_ids, scsi.get_scsi_disk_name(_ids)) )

#Check LUN wwid
scsi_disk = scsi.get_scsi_disk_name(scsi_ids[0])
wwid = scsi.wwid_of_disk(scsi_disk)
if not wwid:
    print("FAIL: Could not find WWID for %s", scsi_disk)
    obj_sanmgmt.lun_remove(tmp_lun)
    sys.exit(1)
print("INFO: New disks have WWID: %s" % wwid)
new_mpath = mp.mpath_name_of_wwid(wwid)
if new_mpath:
    print("INFO: tmp LUN is using mpath: %s" % new_mpath)

print("PASS: LUN %s successfully created" % tmp_lun)
sys.exit(0)
