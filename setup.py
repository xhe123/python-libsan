import sys
from setuptools import setup

install_requires = ["paramiko==1.7.5"]

#install argparse if python < 2.7, since 2.7 argparse is included on standard lib
if sys.hexversion < 0x02070000:
    install_requires.append("argparse")


setup(name='libsan',
      description='Python modules to manage SAN devices',
      version='0.1.1',
      packages=['libsan','libsan/host', 'libsan/switch', 'libsan/switch/cisco', 
                'libsan/switch/brocade', 'libsan/physwitch', 'libsan/physwitch/apcon', 
                'libsan/array', 'libsan/misc', 'libsan/array/dell',
                'libsan/array/linux', 'libsan/array/netapp', 'libsan/array/emc'],
      install_requires=install_requires,
      #data_files=[('/etc', ['sample_san_top.conf'])],
      scripts=['bin/sancli'],
      test_suite = 'tests',
      url='https://gitlab.com/bgoncalv/python-libsan.git',
      author='Bruno Goncalves',
      author_email='bgoncalv@redhat.com',
      #long_description=open("README.md").read()
      )
